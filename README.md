

Amazon SQS Extended Client Library for PHP
==========================================

You can install this library using composer doing:

```
composer require fahmy/amazon-sqs-php-extended-client-lib
```